<%-- 
    Document   : index
    Created on : 11-abr-2016, 16:51:54
    Author     : alumno
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <title>Iniciar sesion</title>
        <link rel="stylesheet" href="css/reset.css">
        <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
        <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
        <link rel="stylesheet" href="css/style.css">
        <title>Iniciar Sesion</title>
        
        <%
            
            if(session.getAttribute("usuario")!=null){
                response.sendRedirect("bienvenido.jsp");
            }
        %>
    </head>
   <body>
        <div class="pen-title">
            <h1></h1>
        </div>

        <div class="container">
            <div class="card"></div>
            <div class="card">
                <h1 class="title">Login</h1>
                <form method="post" action="IniciarSesion">
                     <input type="hidden" value="login" name="flag" id="flag"/>
                    <div class="input-container">
                        <input type="text" id="Username" name="txtUser"required="required"/>
                        <label for="Usuario">Nombre de usuario</label>
                        <div class="bar"></div>
                    </div>
                    <div class="input-container">
                        <input type="password" id="Password" name="txtPass"required="required"/>
                        <label for="Password">Password</label>
                        <div class="bar"></div>
                    </div>
                    <div class="button-container">
                        <button><span>Log in</span></button>                        
                    </div> 
                </form>
            </div>
           
        </div>
         




    </body>
</html>
