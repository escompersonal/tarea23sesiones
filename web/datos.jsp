<%-- 
    Document   : bienvenido
    Created on : 11-abr-2016, 17:20:05
    Author     : alumno
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/reset.css">
        <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
        <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
        <link rel="stylesheet" href="css/style.css">
        <title>Datos</title>
    </head>
    <body>
        <header>
            <div id='cssmenu'>
                <ul>
                    <li><a href='bienvenido.jsp'>Home</a></li>
                    <li class='active'><a href='datos.jsp'>Datos</a></li>
                </ul>
            </div>

        </header>
        <div class="pen-title">
            <p >
                <%
                    String nombre = (String) session.getAttribute("usuario");
                    out.print("Bienvenido " + nombre);
                %>
            <p>
        </div>
        <div class="container">

            <form action="IniciarSesion" method="post">

                <div class="logout-container">
                    <button><span>Log out</span></button>
                </div>

            </form>

        </div>
    </body>
</html>
