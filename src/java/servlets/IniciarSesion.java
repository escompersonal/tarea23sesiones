/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author alumno
 */
@WebServlet(name = "IniciarSesion", urlPatterns = {"/IniciarSesion"})
public class IniciarSesion extends HttpServlet {

    private final static String LOGIN_NAME = "usuario";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void login(HttpServletRequest request, HttpServletResponse response, String usuario) {//Este medodo coloca el nombre del usuario que recibe en las sesion
        HttpSession sesion = request.getSession();//obtiene la sesion
        sesion.setAttribute(LOGIN_NAME, usuario);//coloca el objeto usuario y l coloca el combre de referencia igual al contenido de la variable LOGIN_NAME
    }

    public void logout(HttpServletRequest request, HttpServletResponse response) {//este metodo cierra las sesion y elimina el atributo LOGIN_NAME
        HttpSession sesion = request.getSession();//Obtiene la sesion
        sesion.removeAttribute(LOGIN_NAME);//remueve el atributo
        if (sesion != null) {// si se obtuvo una sesion
            sesion.invalidate();//entonces la invalida
        }
    }

    public boolean consultarSesion(HttpServletRequest request) {//verifica el estafo de la sesion
        HttpSession sesion = request.getSession();//obtiene la sesion
        if (sesion == null) {//si la sesion esta activa devuelve false
            return false;
        } else {
            return true;
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        String flag = null;//atributo que se utiliza para identificar de donde viene la peticion
        flag = request.getParameter("flag");
        if (flag != null) {//si diferente de nulo, este servlet fue llamado desde el login
            try {
                String user = request.getParameter("txtUser");//obtiene el texto del campo txtUser
                String pass = request.getParameter("txtPass");//obtiene el texto del campo txtPass
                login(request, response, user);//inicia sesion
                response.sendRedirect("bienvenido.jsp");//redirige a la pagina bienvenido.jsp
            } catch (IOException ex) {
                Logger.getLogger(IniciarSesion.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {//de lo contrario el servlet fue llamdo desde el boton de logout
            try {
                logout(request, response);//cierra la sesion
                response.sendRedirect("index.jsp");//redirecciona al index
            } catch (IOException ex) {
                Logger.getLogger(IniciarSesion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
